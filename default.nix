{ pkgs ? import <nixpkgs> {} }:

let
  blackbox-config = pkgs.writeTextFile {
    name = "blackbox.yml";
    text = ''
      modules:
        http_2xx:
          prober: http
          timeout: 5s
          http:
            fail_if_not_ssl: true
            follow_redirects: true
            tls_config:
              insecure_skip_verify: false
            # TODO fix ipv6
            preferred_ip_protocol: ip4
        tcp_connect:
          prober: tcp
          timeout: 5s
          tcp:
            # TODO fix ipv6
            preferred_ip_protocol: ip4
    '';
  };
  prometheus-uptime-rule = pkgs.writeTextFile {
    name = "uptime.rules";
    text = ''
      groups:
      - name: uptime-rules
        rules:
            - alert: site_down
              expr: probe_success == 0
              for: 5m
              labels:
                severity: warning
              annotations:
                description: '{{ $labels.instance }} of job {{ $labels.job }} has
                              been down for more than five minutes.'
                summary: Instance {{ $labels.instance }} is down
    '';
  };
  prometheus-config = pkgs.writeTextFile {
    name = "prometheus.yml";
    text = ''
      global:
          scrape_interval: 15s
          evaluation_interval: 15s
      
          external_labels:
              monitor: "my-domain"
      
      rule_files:
          - "${prometheus-uptime-rule}"
      
      alerting:
          alertmanagers:
              - scheme: http
                static_configs:
                    - targets:
                          - "alertmanager:9093"
      
      scrape_configs:
          - job_name: 'blackbox-http'
            metrics_path: /probe
            params:
              module: [http_2xx]
            static_configs:
              - targets:
                - https://www.eicas.nl
                - https://cloud.eicas.nl
            relabel_configs:
              - source_labels: [__address__]
                target_label: __param_target
              - source_labels: [__param_target]
                target_label: instance
              - target_label: __address__
                replacement: blackbox-exporter:9115
    '';
  };
  blackbox-exporter-container = pkgs.dockerTools.buildLayeredImage {
    name = "raboof/eicas-blackbox-exporter";
    tag = "latest";
    contents = [
      pkgs.cacert
    ];
    config.Cmd = [
      "${pkgs.prometheus-blackbox-exporter}/bin/blackbox_exporter"
      "--config.file=${blackbox-config}"
    ];
  };
  prometheus-container = pkgs.dockerTools.buildLayeredImage {
    name = "raboof/eicas-prometheus";
    tag = "latest";
    config.Cmd = [
      "${pkgs.prometheus}/bin/prometheus"
      "--config.file=${prometheus-config}"
    ];
  };
  grafana-homepath = pkgs.runCommand "grafana-homepath" {} ''
    mkdir -p $out
    ln -s ${pkgs.grafana}/share/grafana/public $out/public
    mkdir $out/conf
    for i in defaults.ini ldap_multiple.toml ldap.toml
    do
      ln -s ${pkgs.grafana}/share/grafana/conf/$i $out/conf/$i
    done
    mkdir $out/conf/provisioning
    cp -r ${./grafana/provisioning}/* $out/conf/provisioning
  '';
  grafana-container = pkgs.dockerTools.buildLayeredImage {
    name = "raboof/eicas-grafana";
    tag = "latest";
    config.Cmd = [
      "${pkgs.grafana}/bin/grafana-server"
      "-homepath" "${grafana-homepath}"
    ];
  };
  alertmanager-config = pkgs.writeTextFile {
      name = "alertmanager.config.yml";
      text = ''
        route:
          receiver: 'default-receiver'
          group_wait: 30s
          group_interval: 5m
          repeat_interval: 12h
        
        receivers:
          - name: 'default-receiver'
            email_configs:
                - send_resolved: true
                  to: \'\'
                  from: 'monitoring@mydomain.com'
                  smarthost: ':587'
                  auth_username: 'hello'
                  auth_password: 'changeme'
      '';
  };
  alertmanager-container = pkgs.dockerTools.buildLayeredImage {
    name = "raboof/eicas-alertmanager";
    tag = "latest";
    config.Cmd = [
      "${pkgs.prometheus-alertmanager}/bin/alertmanager"
      "--config.file=${alertmanager-config}"
    ];
  };
in
  pkgs.writeShellScript "load-all.sh" ''
    set -o pipefail
    docker load < ${blackbox-exporter-container}
    docker load < ${prometheus-container}
    docker load < ${grafana-container}
    docker load < ${alertmanager-container}
    ${pkgs.docker-compose}/bin/docker-compose up
  ''
