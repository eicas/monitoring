The specifications to build a set of docker
images to monitor the EICAS infrastructure.

This is currently an ephemeral setup: metrics
are not persisted across updates.

Based on https://sterba.dev/posts/uptime-monitoring/

## Usage

The main `default.nix` builds a script that loads the
relevant docker images into your local docker instance,
which you can then start with `docker-compose`:

    $ nix-build | xargs sh
